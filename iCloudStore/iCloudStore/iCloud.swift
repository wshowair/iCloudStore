//
//  Store.swift
//  iCloudStore
//
//  Created by innovapost on 2017-09-11.
//  Copyright © 2017 Swifter. All rights reserved.
//

import CloudKit
import RxSwift

public struct ICloudStore {
  static public let zone = CKRecordZone(zoneName: "PersonZone") //have to add it here to have one instance of the zone which is having change token expired error.
  public init() {}
}

public protocol ICloudStorable {
  var uuid:String { get }
  static var iCloudRecordType: String { get }
  static var iCloudZoneName: String { get }
  func toCloudObject()->CKRecord
}

public typealias CKRecordType = String

public enum ICloudPushResult {
  case
  singleRecordResultType(CKRecord),
  allRecordsIDsResultType([CKRecordID]),
  allRecordsResultType([CKRecord])
}

public enum ICloudFetchResult {
  case
  singleRecordResultType(CKRecord),
  compositeRecordID(CKRecordID,CKRecordType)
}

//Add rx namespace
extension ICloudStore: ReactiveCompatible {}

/* https://goo.gl/mGTKSM */
extension ObservableType {
  public func unwrap<T>() -> Observable<T> where E == Optional<T> {
    return self
      .filter { value in
        if case .some = value {
          return true
        }
        return false
      }.map { $0! }
  }
}
