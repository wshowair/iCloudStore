//
//  iCloud+Error+Rx.swift
//  iCloudStore
//
//  Created by innovapost on 2017-11-10.
//  Copyright © 2017 Swifter. All rights reserved.
//

import RxSwift
import CloudKit

extension CKError{
  public static let maxNumberOfRetries = 3
}
extension ObservableType {
  public func recoverFromError() -> Observable<Self.E> {
    return self.recoverFromRetriableError()
      .recoverFromNetworkUnavailable()
      .recoverFromZoneNotFoundError()
      .recoverFromServerChangeTokenExpired()
  }
  
  public func recoverFromRetriableError() -> Observable<Self.E> {
    return self.retryWhen { e in
      return e.enumerated().flatMap { (attempt, error) -> Observable<Int> in
        guard attempt<CKError.maxNumberOfRetries,
          let iCloudError = error as? CKError,
          let delay = iCloudError.retryAfterSeconds else {
            return Observable.error(error)
        }
        return Observable<Int>.timer(delay, scheduler:MainScheduler.instance).take(1)
      }
    }
  }
  
  public func recoverFromNetworkUnavailable() -> Observable<Self.E> {
    return  self.retryWhen({ (error) -> Observable<NetworkStatus>  in
      return error.flatMap({ (error) ->Observable<NetworkStatus>  in
        if NetworkSurveyor.shared.status.value == .reachableViaWiFi {
          return Observable<NetworkStatus>.error(error)
        }
        return NetworkSurveyor.shared.status.asObservable().filter({ (status) -> Bool in
          return status == .reachableViaWiFi
        })
      })
    })
  }
  
  public func recoverFromZoneNotFoundError()->Observable<Self.E>{
    return self.retryWhen({ (errorObservable) -> Observable<CKRecordZoneID> in
      return errorObservable.flatMap({ (error) ->Observable<CKRecordZoneID>  in
        guard let iCloudError = error as? CKError,
          iCloudError.code == .partialFailure,
          let partialErrors = iCloudError.partialErrorsByItemID,
          let _ = partialErrors.filter({ return ($1 as? CKError)?.code == .zoneNotFound}).first?.value as?CKError else {
            return Observable.error(error)
        }
        
        //Make sure that you only have one instance of the zone so that when you reset changeToken, you set it for the right instance
        return ICloudStore.zone.rx.create()
      })
    })
  }
  
  public func recoverFromServerChangeTokenExpired()->Observable<Self.E>{
    //the error could happen only with two operations, fetch changes for record_zone/database.
    return self.retryWhen({ (errorObservable) -> Observable<Int> in
      return errorObservable.flatMap({ (error) -> Observable<Int> in
        guard let iCloudError = error as? CKError,
          iCloudError.code == .changeTokenExpired else{
            return Observable.error(error)
        }
        ICloudStore.zone.changeToken = nil
        return Observable.just(1) //retry again by firing a dummy observable
      })
    })
  }
}
