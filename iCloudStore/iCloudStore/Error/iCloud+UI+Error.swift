//
//  iCloudError.swift
//  iCloudStore
//
//  Created by innovapost on 2017-10-03.
//  Copyright © 2017 Swifter. All rights reserved.
//

import CloudKit


public protocol UIConveyableError {
  var handler: ()->Void { get set }
  func conveyToUI ()->Void
}

extension UIConveyableError {
  public func conveyToUI() {
    self.handler()
  }
}

extension CKError : UIConveyableError{
  
  fileprivate struct CKErrorHandler{
    static var internalHandler: ()->Void = {}
  }
  
  public var handler: ()->Void {
    get {
      return CKErrorHandler.internalHandler
    }
    set (newHandler){
      CKErrorHandler.internalHandler = newHandler
    }
  }
}
