//
//  iCloud+Zone.swift
//  iCloudStore
//
//  Created by innovapost on 2017-09-25.
//  Copyright © 2017 Swifter. All rights reserved.
//

import CloudKit
import RxSwift

extension Reactive where Base == ICloudStore{
  public func fetchAllZones()->Observable<[CKRecordZone]>{
    let operation = CKFetchRecordZonesOperation.fetchAllRecordZonesOperation()
    return Observable.create({ (observer) -> Disposable in
      operation.fetchRecordZonesCompletionBlock = { zones , error in
        if let error = error { observer.onError(error) }
        else if let zones = zones { observer.onNext(Array<CKRecordZone>(zones.values)) }
        observer.onCompleted()
      }
      CKContainer.default().privateCloudDatabase.add(operation)
      return Disposables.create()
    })
  }
}
