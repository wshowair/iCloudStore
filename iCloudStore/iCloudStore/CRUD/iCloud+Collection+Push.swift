//
//  iCloud+Collection+Push.swift
//  iCloudStore
//
//  Created by innovapost on 2017-09-21.
//  Copyright © 2017 Swifter. All rights reserved.
//

import CloudKit
import RxSwift

extension Reactive where Base == ICloudStore {
  public func push(_ storables:[ICloudStorable], usingOperationConfiguration configuration:CKOperationConfiguration?=nil)->Observable<CKRecord>{
    return self.push(storables: storables, usingOperationConfiguration: configuration)
      .map({ (result) -> CKRecord? in
        switch result{
        case .singleRecordResultType(let record):
          return record
        default:
          return nil
        }
      })
      .unwrap()
  }
}

