//
//  CKSubscription+Rx.swift
//  iCloudStore
//
//  Created by innovapost on 2017-09-30.
//  Copyright © 2017 Swifter. All rights reserved.
//

import CloudKit
import RxSwift

/* Notes from CKSubscription API documentation:
 * 1. Subscription objects do not become active until you save them to the server and the server has time to index them.
 * 2. Most of the configuration for subscription objects happens at initialization time. The only other piece of configuration to perform is to specify how to deliver push notifications to the user’s device. Use the notificationInfo property of this object to configure the push notification delivery options.
 * 2.1 If you do not assign a value to this property, the server still sends push notifications to your app but those notifications do not cause the system to alert the user. The default value of this property is nil.
 * 3. Subscriptions must be created in the Development environment first and then promoted to Production. Attempting to create a subscription directly in the Production environment will result in an error.
 * 4. Because push notifications have a limited payload size, the server may omit keys and other pieces of data to keep the payload under the maximum limit.
 * 5. If that happens, you can fetch the entire data payload from the server with a CKFetchNotificationChangesOperation object. That operation object returns instances of the CKQueryNotification or CKRecordZoneNotification classes that provide information about the push notifications that were delivered to your app.
 */

/* My Notes:
 * 1. To handle a push notification, create CKNotification object from the userInfo dictionary that is received in application(_:didReceiveRemoteNotification:fetchCompletionHandler:) method of your app delegate.
 * 2. By default APN server would send recordID that causes the push notification to fire however, you can ask the server to send more data by setting desiredKeys property of notificationInfo object that is used to describe the delivery options for a subscription.
 * 3. When you initialize a subscription delivery options/ custom data you use an object of class CKNotificationInfo but when you handle the receiption of a subscription, you use an object of class CKNotification instead.
 * 4. If notificationInfo is not set for a subscription, iCloud Server will reject to create it
 */

extension Reactive where Base: CKSubscription {
  public func create<Base>(usingOperation operation:CKModifySubscriptionsOperation = CKModifySubscriptionsOperation() )->Observable<Base>{
    return Observable.create({ (observer:AnyObserver) -> Disposable in
      operation.subscriptionsToSave = [self.base]
      operation.modifySubscriptionsCompletionBlock = { subscriptions, _ , error in
        if let error = error { observer.onError(error) }
        else if let subscriptions = subscriptions { observer.onNext(subscriptions.first! as! Base) }
        observer.onCompleted()
      }
      CKContainer.default().privateCloudDatabase.add(operation)
      return Disposables.create()
    })
  }
  
  public func delete(usingOperation operation:CKModifySubscriptionsOperation = CKModifySubscriptionsOperation() )->Observable<String>{
    return Observable.create({ (observer) -> Disposable in
      operation.subscriptionIDsToDelete = [self.base.subscriptionID]
      operation.modifySubscriptionsCompletionBlock = { _, subscriptionsIDs , error in
        if let error = error { observer.onError(error) }
        else if let subscriptionsIDs = subscriptionsIDs { observer.onNext(subscriptionsIDs.first!) }
        observer.onCompleted()
      }
      CKContainer.default().privateCloudDatabase.add(operation)
      return Disposables.create()
    })
  }
  
  public func fetch(usingOperation operation:CKFetchSubscriptionsOperation = CKFetchSubscriptionsOperation())->Observable<CKSubscription>{
    return Observable.create({ (observer) -> Disposable in
      operation.subscriptionIDs = [self.base.subscriptionID]
      operation.fetchSubscriptionCompletionBlock = { dictionary, error in
        if let error = error { observer.onError(error) }
        else if let dictionary = dictionary { observer.onNext(dictionary.first!.value) }
        observer.onCompleted()
      }
      CKContainer.default().privateCloudDatabase.add(operation)
      return Disposables.create()
    })
  }
}
