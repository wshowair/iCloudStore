//
//  NetworkSurveyor.swift
//  iCloudStore
//
//  Created by innovapost on 2017-11-03.
//  Copyright © 2017 Swifter. All rights reserved.
//

import RxSwift

#if DEBUG
  extension NetworkStatus: CustomStringConvertible{
    public var description:String {
      switch self {
      case .notReachable:
        return "Not Reachable"
      case .reachableViaWiFi:
        return "Reachable via WiFi"
      case .reachableViaWWAN:
        return "Reachable via Cellular"
      }
    }
  }
#endif

public class NetworkSurveyor {
  public static let shared = NetworkSurveyor()
  private let networkReachability: Reachability?
  public private(set) var status: Variable<NetworkStatus>
  private init(){
    networkReachability = Reachability.forInternetConnection()
    status = Variable(NetworkStatus.reachableViaWiFi)
  }
  
  public   func start(){
    NotificationCenter.default.addObserver(self, selector: #selector(handleChanges(_:)), name: NSNotification.Name(rawValue: "kNetworkReachabilityChangedNotification"), object: nil)
    networkReachability?.startNotifier()
  }
  
  public func stop(){
    NotificationCenter.default.removeObserver(self)
    networkReachability?.stopNotifier()
  }
  
  @objc func handleChanges(_ notification : Notification) {
    let reachability = notification.object as? Reachability
    guard let status = reachability?.currentReachabilityStatus() else { return  }
    
    if status != self.status.value {
      self.status.value = status //trigger observers about the network change.
    }
  }
  
  deinit {
    self.stop()
  }
}
