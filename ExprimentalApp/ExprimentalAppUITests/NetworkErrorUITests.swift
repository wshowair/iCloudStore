//
//  AppNoNetworkUITests.swift
//  ExprimentalAppUITests
//
//  Created by innovapost on 2017-10-30.
//  Copyright © 2017 Swifter. All rights reserved.
//

import XCTest

//Only compile for iOS Devices. Source: https://goo.gl/JdgtnE -> Preprocessor Directives -> Conditional Compilation Blocks
#if (arch(arm) || arch(arm64)) && os(iOS)
  class NetworkErrorUITests: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUp() {
      super.setUp()
      // In UI tests it is usually best to stop immediately when a failure occurs.
      continueAfterFailure = false
      app.launch()
    }
    
    override func tearDown() {
      Settings.app.terminate()
      super.tearDown()
    }
    
    func testNetworkNotAvailable() {
      Settings.app.launch()
      XCTContext.runActivity(named: "Turn On Airplane mode in Settings App") { _ in
        Settings.Airplane.turnOn()
      }
      app.activate()
      
      //Detect network status change to not reachable
      var networkStatusLabel = app.staticTexts["Not Reachable"]
      XCTestAssertExistence(of: networkStatusLabel)
      
      //Generate network unavailable error
      app.buttons["Net unavailable"].tap()
      let errorLabel = app.staticTexts["Network Unavailable"]
      XCTestAssertExistence(of:errorLabel)
      // Restore internet connectivity
      XCTContext.runActivity(named: "Turn Off Airplane mode in Settings App") { _ in
        Settings.Airplane.turnOff()
      }
      app.activate()
      //Detect network status change to reachable via WiFi
      networkStatusLabel = app.staticTexts["Reachable via WiFi"]
      XCTestAssertExistence(of: networkStatusLabel)
    }
    
    func testNetworkFailure() {
      app.buttons["Net Fail"].tap()
      let errorLabel = app.staticTexts["Network Failure"]
      XCTestAssertExistence(of: errorLabel)
    }
  }
#endif
