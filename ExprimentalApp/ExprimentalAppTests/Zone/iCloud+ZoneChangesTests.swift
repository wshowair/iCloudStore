//
//  iCloudTests.swift
//  ExprimentalAppTests
//
//  Created by innovapost on 2017-09-28.
//  Copyright © 2017 Swifter. All rights reserved.
//

import XCTest
import CloudKit
import RxSwift

@testable import ExprimentalApp
@testable import iCloudStore

class iCloud_ZoneChangesTests: ICloudTestCase {
  var zoneChangeToken :CKServerChangeToken?
  
  override func tearDown() {
    UserDefaults.standard.removeObject(forKey: iCloudZone.ZONE_SERVER_CHANGE_TOKEN_KEY)
    super.tearDown()
  }
  
  func testServerChangeTokenUpdate() {
    XCTAssertNil(iCloudZone.changeToken)
    let expectation = XCTestExpectation(description: "Zone Change Token is updated")
    self.iCloudZone.rx.fetchChanges(since:iCloudZone.changeToken)
      .subscribe(
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted: { expectation.fulfill()}
      )
      .disposed(by: bag)
    wait(for: [expectation], timeout: defaultTimeout)
    XCTAssertNotNil(iCloudZone.changeToken)
  }
  
  
  func testZoneChangesIncludeModifiedRecordsOnly(){
    let expectation = XCTestExpectation(description: "Modifed Records are downloaded")
    self.iCloudZone.rx.fetchChanges(since:iCloudZone.changeToken)
      .subscribe(
        onNext:{ (change:ICloudFetchResult) in
          switch change{
          case .singleRecordResultType(let record):
            XCTAssert(self.persons.contains(record.toDataObject()))
          case .compositeRecordID(_ , _):
            XCTAssert(false, "No record should have been deleted")
          }
      },
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted: { expectation.fulfill() }
      )
      .disposed(by: bag)
    wait(for: [expectation], timeout: defaultTimeout)
  }
  
  func testZoneChangesIncludeModifiedAndDeletedRecords() {
    //Step 1: Push 4 records, fetch zone changes, must have those four records
    self.testZoneChangesIncludeModifiedRecordsOnly()
    //Step 2: Push 1 record, delete 1 record, fetch zone changes must have both changes.
    let expectation = XCTestExpectation(description: "Modifed/Deleted Records are downloaded")
    let newPerson = Person(name: "name-5", age: 13, uuid: UUID().uuidString)
    self.iCloud.rx.push(storables: [newPerson], andDelete: [self.persons.first!])
      .flatMapFirst { _ -> Observable<ICloudFetchResult> in
        return self.iCloudZone.rx.fetchChanges(since:self.iCloudZone.changeToken)
      }
      .subscribe(
        onNext:{ (change:ICloudFetchResult) in
          switch change{
          case .singleRecordResultType(let record):
            XCTAssert(record.recordID.recordName == newPerson.uuid)
          case .compositeRecordID(let recordID , let recordType):
            XCTAssert(recordType == "\(Person.self)")
            XCTAssert(recordID.recordName == self.persons.first!.uuid)
          }
      },
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted: { expectation.fulfill() }
      )
      .disposed(by: bag)
    wait(for: [expectation], timeout: defaultTimeout)
  }
}

