//
//  iCloud+SubscriptionTests.swift
//  ExprimentalAppTests
//
//  Created by innovapost on 2017-09-25.
//  Copyright © 2017 Swifter. All rights reserved.
//

import XCTest
import CloudKit
import iCloudStore
import RxSwift

@testable import ExprimentalApp

class iCloud_SubscriptionTests: ICloudTestCase {
  //MARK:- Added to increase Coverage.
  //Deleting a zone, deletes any subscriptions registered with the zone.
  override func tearDown() {
    let expectation = XCTestExpectation(description: "iCloud Subscription: TearDown is completed")
    self.iCloudSubscription.rx.delete()
      .subscribe(
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted: { expectation.fulfill() }
      )
      .disposed(by: bag)
    super.tearDown()
  }
  
  func testFetchAllSubcriptions() {
    let expectation = XCTestExpectation(description: "iCloud Subscription: fetch All is completed")
    self.iCloud.rx.fetchAllSubscriptions()
      .subscribe(
        onNext: { XCTAssert($0.first!.subscriptionID == self.iCloudSubscription.subscriptionID) },
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted: { expectation.fulfill() }
      )
      .disposed(by: bag)
    wait(for: [expectation], timeout: defaultTimeout)
  }
  
  func testFetchSubscription() {
    let expectation = XCTestExpectation(description: "iCloud Subscription: fetch is completed")
    self.iCloudSubscription.rx.fetch()
      .subscribe(
        onNext: { XCTAssert($0.subscriptionID == self.iCloudSubscription.subscriptionID) },
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted: { expectation.fulfill() }
      )
      .disposed(by: bag)
    wait(for: [expectation], timeout: defaultTimeout)
  }
}
