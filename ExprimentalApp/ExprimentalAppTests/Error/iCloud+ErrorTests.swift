//
//  iCloud+ErrorTests.swift
//  ExprimentalAppTests
//
//  Created by innovapost on 2017-10-04.
//  Copyright © 2017 Swifter. All rights reserved.
//

import XCTest
import CloudKit
import iCloudStore
import RxSwift

@testable import ExprimentalApp

class iCloud_ErrorTests: ICloudTestCase {
  let sameUUID = UUID().uuidString
  
  func testPushingSameRecordTwice() {
    let persons = [
      Person(name: "name-1", age: 20, uuid: sameUUID),
      Person(name: "name-2", age: 30, uuid: sameUUID),
      ]
    
    let expectation = XCTestExpectation(description: "invalid arguments error is received")
    self.iCloud.rx.push(persons)
      .subscribe(
        onError: { error in
          let cloudErrorCode = CKError.init(_nsError: error as NSError).errorCode//"You can't save the same record twice: <CKRecordID:.......>"
          XCTAssert(cloudErrorCode == 12) //InvalidArguments Error
          expectation.fulfill()
      }
      ).disposed(by: bag)
    
    wait(for: [expectation], timeout: defaultTimeout)
  }
  
  //  func testRequestRateLimitedByPushingRecords() {
  //    let expectation = XCTestExpectation(description: "request rate limited error is received")
  //    for _ in 0...10000 {
  //      let person = [Person(name: "new-name", age: 90, uuid: UUID().uuidString)]
  //      iCloud.rx.push(person).subscribe(onError: { error in
  //          print(error)
  //          expectation.fulfill()
  //      })
  //      .disposed(by: bag)
  //    }
  //
  //    wait(for: [expectation], timeout: 120)
  //  }
}
