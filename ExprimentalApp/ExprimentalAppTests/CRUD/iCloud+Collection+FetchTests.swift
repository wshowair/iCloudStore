//
//  iCloud+Collection+FetchTests.swift
//  ExprimentalAppTests
//
//  Created by innovapost on 2017-09-22.
//  Copyright © 2017 Swifter. All rights reserved.
//

import XCTest
import CloudKit
import iCloudStore
import RxSwift

@testable import ExprimentalApp

class iCloud_Collection_FetchTests: ICloudTestCase {
  
  func testFetchExistingCollection() {
    let expectation = XCTestExpectation(description: "iCloud collection fetch: response is received")
    let expectedIDs = persons.map{$0.uuid}
    self.iCloud.rx.fetch(persons)
      .subscribe(
        onNext: { XCTAssert(expectedIDs.contains($0.recordID.recordName)) },
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted:{ expectation.fulfill() }
      )
      .disposed(by:bag)
    
    wait(for: [expectation], timeout: defaultTimeout)
  }
}
