//
//  iCloud+Collection+DeleteTests.swift
//  ExprimentalAppTests
//
//  Created by innovapost on 2017-09-22.
//  Copyright © 2017 Swifter. All rights reserved.
//

import XCTest
import CloudKit
import iCloudStore
import RxSwift

@testable import ExprimentalApp

class iCloud_Collection_DeleteTests: ICloudTestCase {
  
  func testDeleteExistingCollection() {
    let expectation = XCTestExpectation(description: "iCloud collection deletion: response is received")
    let expectedIDs = persons.map{$0.uuid}
    self.iCloud.rx.delete(persons)
      .subscribe(
        onNext: { for recordID in $0 { XCTAssert(expectedIDs.contains(recordID.recordName)) } },
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted:{ expectation.fulfill() }
      )
      .disposed(by:bag)
    
    wait(for: [expectation], timeout: defaultTimeout)
  }
}
